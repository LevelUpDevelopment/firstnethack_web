import React from 'react';
import MapGL, { CustomLayer, Marker } from '@urbica/react-map-gl';
import { MapboxLayer } from '@deck.gl/mapbox';
import {PolygonLayer} from '@deck.gl/layers';
import { ScatterplotLayer } from '@deck.gl/layers';
import 'mapbox-gl/dist/mapbox-gl.css';
import neighborhoodCoordinates from './Geometries.js';
import _ from 'lodash';

class Map extends React.Component {

  constructor(props) {
    super(props);

    this.mapboxAccessToken = 'pk.eyJ1IjoibGV2ZWx1cGRldmVsb3BtZW50IiwiYSI6ImNrMGoxcHpnbTA2NDMzYm8zMWo5YmtnMzkifQ.8BO1GnrcFgGHZeNIsl8cUg';
    this.neighborhoodCoordinates = neighborhoodCoordinates;

    this.state = {
      viewport: {
        longitude: -86.238064,
        latitude: 39.796268,
        zoom: 16,
      },
      neighborhoodBlocks: JSON.parse(JSON.stringify(neighborhoodCoordinates)).map(c => {
        return {contour: c, layerKey: `Layer_${Math.random()*10000000}`}
      }),
      incidents: {},
      activeIncidentMarker: null,
      activeIncidentMarkerId: null,
    }

    this.myDeckLayer = new MapboxLayer({
      id: 'my-scatterplot',
      type: ScatterplotLayer,
      data: [{ position: [-86.238064, 39.796268], size: 5 }],
      getPosition: (d) => d.position,
      getRadius: (d) => d.size,
      getColor: [255, 0, 0]
    });

    this.incidentTypeNames = {
      'opioids': 'Drugs / Opioids',
      'cartheft': 'Car Theft',
      'breakingandentering': 'Breaking and Entering',
      'pettytheft': 'Petty Theft',
      'robbery': 'Robbery',
      'armedrobery': 'Armed Robbery',
      'armedassault': 'Armed Assault',
      'missingperson': 'Missing Person',
      'amberalert': 'Amber Alert',
      'silveralert': 'Silver Alert',
      'bluealert': 'Blue Alert',
      'domesticviolence': 'Domestic Violence',
    };

    this.serverIp = '10.34.240.151';
  }

  componentDidMount() {
    setInterval(() => {
      const self = this;
      return fetch(`http://${this.serverIp}:1337/api/v1/incident/getIncidents`, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json',
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
      })
      .then(response => response.json())
      .then(
        serverIncidents => {
          if (!serverIncidents) return;

          // Copy the layers
          const neighborhoodBlocks = JSON.parse(JSON.stringify(this.state.neighborhoodBlocks));
          const incidents = JSON.parse(JSON.stringify(this.state.incidents));

          // Go through all incidents and see if they match neighborhood coordinates
          for (let serverIncident of serverIncidents) {
            const incidentCoords = JSON.parse(serverIncident.coords);

            // Skip the below for loop if the incident:
            // * Already exists
            // * Hasn't changed type
            // * Hasn't received any new comments
            if (incidents[serverIncident.id] &&
              incidents[serverIncident.id].data.type === serverIncident.type &&
              _.isEqual(incidents[serverIncident.id].data.incidentComments, serverIncident.incidentComments)) {
              continue;
            }
            for (let i = 0; i < self.neighborhoodCoordinates.length; i += 1) {

              // x[0] or y[1] incident coordinate === neighborhood[j], first coordinate[0], x[0] or y[1]...
              // And is a new incident?
              if (incidentCoords[0] === self.neighborhoodCoordinates[i][0][0] &&
                  incidentCoords[1] === self.neighborhoodCoordinates[i][0][1]) {

                incidents[serverIncident.id] = {
                  data: serverIncident,
                  layerKey: `Map_${Math.random()*10000000}`
                }

                // Find the center of an incident based on the block it belongs in
                {
                  let xSum = 0;
                  let ySum = 0;
                  for (let j = 0; j < self.neighborhoodCoordinates[i].length; j += 1) {
                    xSum += self.neighborhoodCoordinates[i][j][0];
                    ySum += self.neighborhoodCoordinates[i][j][1];
                  }
                  const xAverage = xSum / self.neighborhoodCoordinates[i].length;
                  const yAverage = ySum / self.neighborhoodCoordinates[i].length;

                  incidents[serverIncident.id].data.coords = [xAverage, yAverage];
                }

                // Set color depending on incident type
                let color = null;
                switch (serverIncident.type) {
                case 'incidentfree':
                  color = [0, 0, 255, 30];
                  break;
                case 'cartheft':
                case 'pettytheft':
                  color = [200, 200, 0, 60];
                  break;
                case 'opioids':
                  color = [150, 0, 150, 85];
                  break;
                case 'domesticviolence':
                  color = [250, 0, 0, 140];
                  break;
                default:
                  color = [200, 50, 0, 60];
                  break;
                }

                console.log('Incident at block ' + i);

                // Replace incident at index with new one
                neighborhoodBlocks[i] = {
                  contour: self.neighborhoodCoordinates[i],
                  color,
                  layerKey: `Map_${Math.random()*10000000}`
                };
              }
            }
          }

          this.setState({
            neighborhoodBlocks,
            incidents
          });
          if (this.state.activeIncidentMarker) {
            this.setState({
              activeIncidentMarker: incidents[this.state.activeIncidentMarkerId].data
            })
          }
        }
      )
      .catch(e => {
        console.log(e);
      });
    }, 3000);
  }

  getNeighborhoodLayers(neighborhoodBlocks) {
    const neighborhoodLayers = [];
    for (let i = 0; i < neighborhoodBlocks.length; i += 1) {
      const neighborhoodBlock = neighborhoodBlocks[i];
      let color = [0, 0, 255, 30];
      /*
      if (i % 3 === 1) {
        color = [0, 255, 0, 30];
      } else if (i % 3 === 2) {
        color = [255, 0, 0, 30];
      } */

      neighborhoodLayers.push(<CustomLayer key={neighborhoodBlock.layerKey} layer={new MapboxLayer({
        id: 'neighborhoodBlock'+i,
        type: PolygonLayer,
        data: [{
          contour: neighborhoodBlock.contour
        }],
        filled: true,
        getPolygon: d => d.contour,
        getFillColor: neighborhoodBlock.color || color,
        getLineColor: [0, 0, 0, 0],
        extruded: false
      })}/>);
    }

    return neighborhoodLayers;
  }

  getIncidentMarkers(incidents) {
    const incidentMarkers = [];
    for (let id in incidents) {
      if (Object.prototype.hasOwnProperty.call(incidents, id)) {
        const coords = incidents[id].data.coords;
        if (incidents[id].data.type !== 'incidentfree') {
          incidentMarkers.push(
            <Marker key={incidents[id].layerKey} longitude={coords[0]} latitude={coords[1]}>
              <div className={'marker-container'} style={{ fontSize: '12px', fontWeight: '800', cursor: 'pointer', backgroundColor: 'rgba(200, 200, 200, .4)', borderRadius: '5px', padding: '5px' }}
                onClick={() => this.onClickIncidentMarker(incidents[id].data, id)}>
                {this.incidentTypeNames[incidents[id].data.type]}
              </div>
            </Marker>
          );
        }
      }
    }
    return incidentMarkers;
  }

  onClickIncidentMarker(incidentMarker, id) {
    this.setState({
      activeIncidentMarker: incidentMarker,
      activeIncidentMarkerId: id
    });
  }

  render() {
    return <div style={{ display: 'flex', flexDirection: 'row', minWidth: '800px' }}>
      <MapGL key={this.state.mapsKey}
        style={{ display: 'flex', flexDirection: 'column', flex: '0.67', height: '100vh' }}
        mapStyle='mapbox://styles/mapbox/light-v9'
        accessToken={this.mapboxAccessToken}
        longitude={this.state.viewport.longitude}
        latitude={this.state.viewport.latitude}
        zoom={this.state.viewport.zoom}
        onViewportChange={(viewport) => this.setState({ viewport })}
      >
        {/*<CustomLayer layer={this.highlightLayer} />*/}
        <CustomLayer layer={this.myDeckLayer} />
        {this.getNeighborhoodLayers(this.state.neighborhoodBlocks)}
        {this.getIncidentMarkers(this.state.incidents)}
      </MapGL>
      <div style={{
        display: 'flex',
        flexDirection: 'column',
        flex: '0.33',
        maxHeight: '100vh',
        backgroundColor: '#333',
        padding: '1rem',
        justifyContent: 'space-between'}}>
        {!this.state.activeIncidentMarker &&
        <div style={{padding: '10px', borderRadius: 5}}>
        </div>
        }
        {this.state.activeIncidentMarker &&
        <div style={{padding: '10px', borderRadius: 5, backgroundColor: 'white'}}>
          <h3>[{this.state.activeIncidentMarker.caseNumber}] - {this.incidentTypeNames[this.state.activeIncidentMarker.type]}</h3>
          <div>
            <span style={{ fontWeight: '600' }}>DESCRIPTION:</span>
            &nbsp;
            <span>{this.state.activeIncidentMarker.description || 'N/A'}</span>
          </div>
          <div>
            <span>TIME:</span>
            &nbsp;
            <span> {new Date(this.state.activeIncidentMarker.createdAt).toLocaleString()}</span>
          </div>
          <div>
            <span>ORIGIN:</span>
            &nbsp;
            <span> {this.state.activeIncidentMarker.coords.toString()}</span>
          </div>
          <hr />
          <div>
            <span style={{ fontWeight: '600' }}>TRANSCRIPTION / COMMENTS</span>
            &nbsp;
            {this.state.activeIncidentMarker.incidentComments.map(c => {
              return <div>
                  <div>
                  <span>{c.comment}</span>
                </div>
                <div>
                  <span>{new Date(c.createdAt).toLocaleString()}</span>
                </div>
              </div>;
            })}
          </div>
          <hr />
          <div>
            <span style={{ fontWeight: '600' }}>FOLLOW-UP</span>
            {(this.state.activeIncidentMarker.type !== 'opioids' && this.state.activeIncidentMarker.type !== 'domesticviolence') &&
              <div>
                <div style={{ cursor: 'pointer', padding: '12px', marginTop: '10px', backgroundColor: '#d0d0f0', borderRadius: 5, color: 'black' }}>
                  View Tips
                </div>
              </div>
            }
            {this.state.activeIncidentMarker.type === 'opioids' &&
              <div>
                <div style={{ cursor: 'pointer', padding: '12px', marginTop: '10px', backgroundColor: '#d0d0f0', borderRadius: 5, color: 'black' }}>
                  View Tips
                </div>
                <div style={{ cursor: 'pointer', padding: '12px', marginTop: '10px', backgroundColor: '#d0d0f0', borderRadius: 5, color: 'black' }}>
                  Medical Check
                </div>
                <div style={{ cursor: 'pointer', padding: '12px', marginTop: '10px', backgroundColor: '#d0d0f0', borderRadius: 5, color: 'black' }}>
                  Community Resources
                </div>
              </div>
            }
            {this.state.activeIncidentMarker.type === 'domesticviolence' &&
              <div>
                <div style={{ cursor: 'pointer', padding: '12px', marginTop: '10px', backgroundColor: '#d0d0f0', borderRadius: 5, color: 'black' }}>
                  View Tips
                </div>
                <div style={{ cursor: 'pointer', padding: '12px', marginTop: '10px', backgroundColor: '#d0d0f0', borderRadius: 5, color: 'black' }}>
                  Medical Check
                </div>
                <div style={{ cursor: 'pointer', padding: '12px', marginTop: '10px', backgroundColor: '#d0d0f0', borderRadius: 5, color: 'black' }}>
                  Food and Housing
                </div>
                <div style={{ cursor: 'pointer', padding: '12px', marginTop: '10px', backgroundColor: '#d0d0f0', borderRadius: 5, color: 'black' }}>
                  Community Resources
                </div>
              </div>
            }
          </div>
        </div>
        }
        <div style={{padding: '10px', borderRadius: 5}}>
          <div style={{ display: 'flex', flexDirection: 'row'}}>
            <span style={{ fontWeight: '800', fontSize: '18px', letterSpacing: 1.1, color: 'white' }}>
              LEGEND
            </span>
          </div>
          <div style={{ display: 'flex', flexDirection: 'row', backgroundColor: 'rgba(185, 185, 255, .8)', padding: '12px', marginTop: '15px', borderRadius: 5}}>
            <span style={{ fontWeight: '800', fontSize: '18px', letterSpacing: 1.1 }}>
              NO INCIDENTS
            </span>
          </div>
          <div style={{ display: 'flex', flexDirection: 'row', backgroundColor: 'rgba(250, 250, 0, .8)', padding: '12px', marginTop: '15px', borderRadius: 5}}>
            <span style={{ fontWeight: '800', fontSize: '18px', letterSpacing: 1.1 }}>
              THEFT
            </span>
          </div>
          <div style={{ display: 'flex', flexDirection: 'row', backgroundColor: 'rgba(250, 0, 250, .8)', padding: '12px', marginTop: '15px', borderRadius: 5}}>
            <span style={{ fontWeight: '800', fontSize: '18px', letterSpacing: 1.1 }}>
              DRUGS / OPIOIDS
            </span>
          </div>
          <div style={{ display: 'flex', flexDirection: 'row', backgroundColor: 'rgba(250, 0, 0, .8)', padding: '12px', marginTop: '15px', borderRadius: 5}}>
            <span style={{ fontWeight: '800', fontSize: '18px', letterSpacing: 1.1 }}>
              DOMESTIC VIOLENCE
            </span>
          </div>
          <div style={{ display: 'flex', flexDirection: 'row', backgroundColor: 'rgba(250, 80, 0, .8)', padding: '12px', marginTop: '15px', borderRadius: 5}}>
            <span style={{ fontWeight: '800', fontSize: '18px', letterSpacing: 1.1 }}>
              OTHER
            </span>
          </div>
        </div>
      </div>
    </div>
  }
}

export default Map;