import React from 'react';
import Map from './Map.js';
import './App.css';

function App() {
  return (
    <div className="App">
      <Map name={"Alex"} />
    </div>
  );
}

export default App;
